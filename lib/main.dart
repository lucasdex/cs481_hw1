import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: Text("Hello World App"),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("My name is ", style: TextStyle(fontSize: 17),),
                    Text(
                      "Lucas ZARKA",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    )
                  ],
                ),
                Container(child: Text("Date: 10/09/2020"), padding: EdgeInsets.only(top: 15, bottom: 50),),
                Container(
                  padding: EdgeInsets.only(left: 30, right: 30),
                  child: Text(
                    "“Sometimes when you innovate, you make mistakes. It is best to admit them quickly, and get on with improving your other innovations.”\n\n        ~ Steve Jobs",
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
